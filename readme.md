# One Pioce

[![GitHub stars](https://img.shields.io/github/stars/le-shi/One.svg?style=social)](https://github.com/le-shi/One/stargazers)
![GitHub All Releases](https://img.shields.io/github/downloads/le-shi/One/total.svg?style=social)
![GitHub release](https://img.shields.io/github/release/le-shi/One.svg?style=social)

[![GitHub issues](https://img.shields.io/github/issues/le-shi/One.svg?style=popout)](https://github.com/le-shi/One/issues)
![GitHub code size in bytes](https://img.shields.io/github/languages/code-size/le-shi/One.svg?color=blueviolet&logoColor=write&style=popout-square)
![GitHub last commit](https://img.shields.io/github/last-commit/le-shi/One.svg)
![GitHub commits since latest release](https://img.shields.io/github/commits-since/le-shi/One/latest.svg)

<!-- [![GitHub license](https://img.shields.io/github/license/le-shi/One.svg?color=red&style=popout)](https://github.com/le-shi/One) -->

---
[我的Hub](https://github.com/le-shi) | [我的笔记](https://github.com/le-shi/One/tree/master/blogs) | [我的简书](https://www.jianshu.com/u/e5a52550a07a)

<!-- <kbd>Enter</kbd> -->

<!-- **configue**

|via|exp|
|-|-|
|xshell5-XTerm.xcs| color XTerm|
|*.ssf|搜狗输入法皮肤|

|1|2|
|-|-|
|CQRS||
|DDD||
|SOA||
|AOP||
|OOP||
|DAO||
|ORM||
|MVC||
|Spring||
|参考文档|https://www.jdon.com/ <br> https://dddcommunity.org/|-|-|-| -->

<!-- |A|B|
|-|-|
|perf|性能剖析工具|
|strace|程序调试工具|
|tcpdump|linux抓包| -->

- bash: sh脚本
- blogs: 笔记
- docker: 开源软件的docker
- opensource: 开源软件的一些资料
- pi: 树莓派的
- python: py脚本
- zabbix: zabbix监控用到的自定义脚本和配置

<!-- ## 树莓派

* [x] 树莓派🥧
* [ ] 猫的 - 自动铲屎😏
* [ ] 猫的 - 自动续粮、续水💧
* [ ] 猫的 - 自动逗猫🐱
* [ ] 我的 - 控制开关灯💡 -->

---
[emoji:+1:](https://www.webfx.com/tools/emoji-cheat-sheet/)
