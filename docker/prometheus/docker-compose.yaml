version: '2'

networks:
  ops:
    external: true

services:
  prometheus:
    container_name: prometheus
    image: prom/prometheus:v2.32.1
    hostname: prometheus-server
    restart: always
    ports:
      - "9090:9090"
    command: ["--config.file=/etc/prometheus/prometheus.yml", "--storage.tsdb.path=/prometheus", "--storage.tsdb.retention.time=15y", "--web.console.libraries=/usr/share/prometheus/console_libraries", "--web.console.templates=/usr/share/prometheus/consoles", "--web.enable-lifecycle"]
    volumes:
      - "./volume/prometheus/data:/prometheus"
      - "./volume/prometheus/config/prometheus.yml:/etc/prometheus/prometheus.yml"
      - "./volume/prometheus/config/targets.yml:/etc/prometheus/targets.yml"
      - "./volume/prometheus/config/rules:/etc/prometheus/rules"
    networks:
      - ops

  pushgateway:
     container_name: pushgateway
     image: prom/pushgateway:v1.4.1
     # --persistence.interval 允许指标暂存，默认保存5分钟，5分钟后，本地存储的指标会删除
     command: --persistence.file=/pushgateway/gateway.db --persistence.interval=3m
     volumes:
       - "./volume/pushgateway/data:/pushgateway"
     hostname: pushgateway
     restart: always
     ports:
       - "9091:9091"
     networks:
       - ops

  alertmanager:
    container_name: alertmanager
    image: prom/alertmanager:v0.22.2
    hostname: alertmanager
    restart: always
    mem_limit: 500M
    ports:
      - "9093:9093"
    volumes:
      - "./volume/alertmanager/data:/data"
      - "./volume/alertmanager/config:/etc/alertmanager"
    networks:
      - ops

  grafana:
    container_name: grafana
    image: grafana/grafana:8.0.6
    hostname: grafana
    restart: always
    user: root
    environment:
      - "GF_SERVER_ROOT_URL=http://grafana.ops.cn"
      - "GF_SECURITY_ADMIN_PASSWORD=admin!@#"
      - "GF_INSTALL_PLUGINS=grafana-clock-panel,grafana-simple-json-datasource,grafana-piechart-panel,redis-datasource"
    ports:
      - "3000:3000"
    volumes:
      - "./volume/grafana/data:/var/lib/grafana"
    networks:
      - ops

  node:
    container_name: node-prom
    image: quay.io/prometheus/node-exporter:v1.1.2
    hostname: ops
    restart: always
    command: --path.rootfs=/host
    ports:
      - "9100:9100"
    volumes:
      - "/:/host:ro,rslave"
    pid: host
    network_mode: host

  cadvisor:
    container_name: cadvisor-prom
    image: google/cadvisor:v0.33.0
    restart: always
    ports:
      - "8080:8080"
    volumes:
      - "/:/rootfs:ro"
      - "/var/run:/var/run:rw"
      - "/sys:/sys:ro"
      - "/var/lib/docker/:/var/lib/docker:ro"
    networks:
      - ops

  mysql-exporter:
    container_name: mysqld-exporter
    image: prom/mysqld-exporter:v0.13.0
    restart: always
    ports:
      - "9104:9104"
    environment:
      - DATA_SOURCE_NAME=exporter:promtheus@(mysql-ip:3306)/
    networks:
      - ops

  redis-exporter:
    container_name: redis-exporter
    image: oliver006/redis_exporter:v1.23.1-amd64
    environment:
      - REDIS_ADDR=redis-ip
      - REDIS_PASSWORD=redis-password
      - REDIS_EXPORTER_INCL_SYSTEM_METRICS=true
    ports:
      - "9121:9121"
    networks:
      - ops

  blackbox:
    container_name: blackbox-exporter
    image: prom/blackbox-exporter:v0.19.0
    command: ["--config.file=/config/blackbox.yml"]
    volumes:
      - "./volume/prometheus/config/blackbox.yml:/config/blackbox.yml"
    restart: always
    ports:
      - "9115:9115"
    networks:
      - ops

  domain:
    container_name: domain-exporter
    image: caarlos0/domain_exporter:v1.12.0
    restart: always
    ports:
      - "9222:9222"
    networks:
      - ops

  vmware:
    container_name: vmware
    image: pryorda/vmware_exporter:v0.16.1
    environment:
      - VSPHERE_USER=username
      - VSPHERE_PASSWORD=password
      - VSPHERE_HOST=vsphere-ip
      - VSPHERE_IGNORE_SSL=True
      - VSPHERE_SPECS_SIZE=2000
    ports:
      - 9272:9272
    network_mode: host

  openstack-exporter:
    container_name: openstack-exporter
    image: quay.io/niedbalski/openstack-exporter-linux-amd64:v1.4.0
    volumes:
      - ./volume/prometheus/config/clouds.yaml:/etc/openstack/clouds.yaml
    ports:
      - 9180:9180
    command: openstack
    networks:
      - ops
