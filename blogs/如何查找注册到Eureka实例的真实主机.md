# 容器化部署时，容器的IP不是真实IP，如何查找注册到Eureka的服务，来自哪个真实主机

1. 在Eureka机器上，使用 tcpdump 命令抓取请求端口是8761的数据: `sudo tcpdump -i eth0 -n port 8761 -w eureka.log`
2. 把数据包文件拿到 wireshark 解析
3. 设置过滤条件: `ip.dst == 10.10.10.14` ，只显示目的地址是Eureka的请求，其中 *10.10.10.14* 是Eureka所在的机器IP地址
4. 导出分析结果: `文件`-`导出分组解析结果`-`为纯文本`，分组格式选择 `详情`-`全部收起`，保存后的文件叫 eureka_request.txt
5. 通过查看文件头信息可以发现，第三个(Source)字段表示源地址，第七个(Info)字段表示请求详情: `head -n 2 eureka_request.txt`
6. 查找符合条件的记录: `awk '/PUT \/eureka\/apps\/大写服务注册名字/{print $3,$8}' eureka_request.txt | awk -F '/' '{print $1,$4}' | sort | uniq`
   1. 查看所有服务的主机IP地址: `awk '/PUT \/eureka\/apps\//{print $3,$8}' eureka_request.txt | awk -F '/' '{print $1,$4}' | sort | uniq`
   2. 同时查看多个指定服务的主机IP地址: `awk '/PUT \/eureka\/apps\//{print $3,$8}' eureka_request.txt | awk -F '/' '{print $1,$4}' | sort | uniq | grep -Ei '服务1|服务2|服务3'`
7. 显示结果是 `172.30.1.9 TEST` 这种形式，第一列就是服务的主机IP地址，第二列是服务名，如果是多副本(多实例)显示的真实主机IP地址可能是多个

## 找到真实IP后，发现不是来自当前环境的服务，如何踢出Eureka(禁止注册)，在这里我们可以叫它 `坏的实例`

1. 通过上面的步骤查出的IP地址
2. 添加 iptables 禁止规则，不需要重启docker或者容器，即时生效

   ```bash
   ## docker-iptables docker处理的iptables规则，不需要宿主机有iptables服务，用法相同. 删除使用 -D
   # 方法1 禁止其他机器访问当前机器的容器，禁止所有访问
   iptables -A DOCKER-USER -s 192.168.9.252 -j DROP
   # 方法2 禁止其他机器访问当前机器的容器，禁止访问某个端口
   iptables -A DOCKER-USER -s 192.168.9.252 -i eth0 -p tcp -m conntrack --ctorigdstport 8761 -j DROP
   ```

3. 抓包验证

   1. 查出 注册中心 容器使用的网卡

      ```bash
      $ docker exec eureka ip a
      1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1000
         link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
         inet 127.0.0.1/8 scope host lo
            valid_lft forever preferred_lft forever
      812: eth0@if813: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1450 qdisc noqueue state UP 
         link/ether 02:42:0a:14:00:73 brd ff:ff:ff:ff:ff:ff
         inet 10.20.0.115/20 brd 10.20.15.255 scope global eth0
            valid_lft forever preferred_lft forever
      848: eth1@if849: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue state UP 
         link/ether 02:42:ac:12:00:1b brd ff:ff:ff:ff:ff:ff
         inet 172.18.0.27/16 brd 172.18.255.255 scope global eth1
            valid_lft forever preferred_lft forever
      ```

   2. 可以看到上面网卡 `eth0@if813` 是绑定的容器本身的IP，网卡 `eth1@if849` 是和本地桥接网络`docker_gwbridge`同网段，根据网卡命名规则得知(猜)对应宿主机的是 `849` 号网卡，查看宿主机的网卡

      ```bash
      $ ip a | grep 849
      849: vethe68816e@if848: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master docker_gwbridge state UP group default 
      ```

   3. 对应宿主机的网卡就是 `vethe68816e@if848` ，真实网卡名是`@`前面的，也就是 `vethe68816e`

   4. 开始抓包，添加禁止规则，看不到流量，就可以啦

      ```bash
      $ sudo tcpdump -i vethe68816e src host 192.168.9.252
      # 现在禁止规则已经添加，应该看不到流量进出
      # 去掉禁止规则，可以看到输出
      ```

4. 禁止规则添加后，`坏的实例` 为什么还在 注册中心 服务列表，什么时候变更状态？

   ```bash
   剔除失效服务间隔时间为90s且存在自我保护的机制

   # 服务参数参考
   # 期望的客户端更新间隔(以秒为单位)(default: 30s)
   eureka.server.expected-client-renewal-interval-seconds=30
   # 指示eureka服务器从收到最后一次心跳开始等待的时间(以秒为单位)(default: 90s)
   eureka.instance.lease-expiration-duration-in-seconds=90
   # 指示eureka客户端需要发送心跳到eureka服务器的频率(以秒为单位)，以表明它仍然活着。(default: 30s)
   # 如果在leaseExpirationDurationInSeconds中指定的时间内没有收到心跳，eureka服务器将从其视图中删除该实例，通过禁止该实例的流量
   eureka.instance.lease-renewal-interval-in-seconds=30
   # 清理无效节点的时间间隔(以毫秒为单位)(default: 6000)
   eureka.server.evictionIntervalTimerInMs=6000
   ```
